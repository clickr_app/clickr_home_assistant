defmodule ClickrHomeAssistant.LocalAdapter do
  use ClickrHomeAssistant.JsonAdapter
  require Logger

  alias ClickrHomeAssistant.{JsonAdapter, RemoteAdapter}

  # Client API
  def start_link(_opts) do
    url = Application.fetch_env!(:clickr_home_assistant, :local_url)
    WebSockex.start_link(url, __MODULE__, :initial,
      name: __MODULE__,
      handle_initial_conn_failure: true
    )
  end

  def send(msg) do
    WebSockex.cast(__MODULE__, {:send, msg})
  end

  # Callbacks (server API)
  @impl WebSockex
  def handle_connect(_conn, :initial) do
    Logger.info("connected")
    {:ok, :initial}
  end

  @impl JsonAdapter
  def handle_json(%{"type" => "auth_required"}, :initial) do
    Logger.debug("authorizing")
    auth_token = Application.fetch_env!(:clickr_home_assistant, :local_auth_token)
    {:reply, %{type: "auth", access_token: auth_token}, :authorizing}
  end

  def handle_json(%{"type" => "auth_invalid"}, :authorizing) do
    {:close, :auth_invalid}
  end

  def handle_json(%{"type" => "auth_ok"}, :authorizing) do
    Logger.debug("subscribing")
    {:reply, %{type: "subscribe_events", id: 1}, :subscribing}
  end

  def handle_json(%{"type" => "result", "id" => 1, "success" => true}, :subscribing) do
    Logger.info("subscribed")
    {:ok, {:subscribed, 2}}
  end

  def handle_json(%{} = msg, {:subscribed, ref}) do
    Logger.debug("received event")
    RemoteAdapter.send("event", msg)
    {:ok, {:subscribed, ref}}
  end

  @impl WebSockex
  def handle_cast({:send, msg}, {:subscribed, ref}) do
    Logger.debug("Sending frame with payload: #{inspect(msg)}")
    msg = Map.put(msg, :id, ref)
    reply(msg, {:subscribed, ref + 1})
  end

  @impl WebSockex
  def handle_disconnect(_conn_status_map, :auth_invalid) do
    Logger.error("auth_invalid -> stop")
    {:ok, :auth_invalid}
  end

  @impl WebSockex
  def handle_disconnect(%{reason: reason}, _status) do
    Logger.error("disconnected -> reconnect (reason: #{inspect(reason)})")
    Process.sleep(1000)
    {:reconnect, :initial}
  end
end
